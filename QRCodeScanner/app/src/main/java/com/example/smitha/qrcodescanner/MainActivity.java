package com.example.smitha.qrcodescanner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    TextView tvuid,tvname,tvgender,tvyob,tvco,tvvtc,tvpo,tvdist,tvstate,tvpc;
    Button buttonScan;
    IntentIntegrator qrScan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         tvuid=(TextView)findViewById(R.id.textViewUID);
         tvname=(TextView)findViewById(R.id.textViewNAME);
        tvgender=(TextView)findViewById(R.id.textViewGENDER);
        tvyob=(TextView)findViewById(R.id.textViewYOB);
        tvco=(TextView)findViewById(R.id.textViewCO);
        tvvtc=(TextView)findViewById(R.id.textViewVTC);
        tvpo=(TextView)findViewById(R.id.textViewPO);
        tvdist=(TextView)findViewById(R.id.textViewDIST);
        tvstate=(TextView)findViewById(R.id.textViewSTATE);
        tvpc=(TextView)findViewById(R.id.textViewPC);
         buttonScan=(Button)findViewById(R.id.buttonScan);
         qrScan=new IntentIntegrator(this);

        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                qrScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//                qrScan.setPrompt("Scan a Aadharcard QR Code");
//                qrScan.setResultDisplayDuration(500);
//                qrScan.setCameraId(0); // Use a specific camera of the device
            qrScan.initiateScan();
            }
        });

    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            if(scanResult.getContents()==null){
                Toast.makeText(this, "Result not found", Toast.LENGTH_SHORT).show();
            }else {
                try {
                    //if scanResult contains data
                    System.out.println("Printing Beneficiary Data");
                    JSONObject jsonObject = new JSONObject(scanResult.getContents());
                    tvuid.setText(jsonObject.getString("uid"));
                    tvname.setText(jsonObject.getString("name"));
                    tvgender.setText(jsonObject.getString("gender"));
                    tvyob.setText(jsonObject.getString("yob"));
                    tvco.setText(jsonObject.getString("co"));
                    tvvtc.setText(jsonObject.getString("vtc"));
                    tvpo.setText(jsonObject.getString("po"));
                    tvdist.setText(jsonObject.getString("dist"));
                    tvstate.setText(jsonObject.getString("state"));
                    tvpc.setText(jsonObject.getString("pc"));
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }else{
            super.onActivityResult(requestCode,resultCode,intent);
        }
        // else continue with any other code you need in the method

    }

}
